# !/usr/bin/env python3
"""
El programa tiene que realizar las funciones de suma, resta, multiplicación o
división en función de lo que diga el usuario
"""

import sys
import operator

operaciones = {
    "sumar": operator.add,
    "restar": operator.sub,
    "multiplicar": operator.mul,
    "dividir": operator.truediv,
}

#Comprobamos lectura de argumentos
if len(sys.argv) != 4 :
    sys.exit("Introduce 'calculadora.py funcion operando1 operando2'")

_, funcion, operando1, operando2 = sys.argv

try:
    operando1 =  float(sys.argv[2])
    operando2 =  float(sys.argv[3])

except ValueError:
    sys.exit("Introduce un número en las dos ultimas posiciones")

if funcion not in operaciones:
    sys.exit("Introduce una operacion: add, sub, mul o div")

#Realizamos la operacion
try:
    resultado = operaciones[funcion](operando1,operando2)
except ZeroDivisionError:
    sys.exit("No se puede dividir entre 0")

print("el resultado es ", resultado)
